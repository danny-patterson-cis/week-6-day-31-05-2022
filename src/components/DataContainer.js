import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { fetchData } from '../redux'


function DataContainer({data,fetchData}) {
 useEffect(()=>{
     fetchData()
 },[])
  return data.loading?(
    <h2>Loading</h2>
  ): data.error ? (
      <h2>{data.error}</h2>
  ):(
      <div>
          <h1>Data List</h1>
          <div>{
              data && 
              data.data &&
              data.data.map(d=><img src={d.url} ></img>)
              }</div>
      </div>
  )
}const mapStateToProps = state => {
    return {
      data: state.data
    }
  }
  
  const mapDispatchToProps = dispatch => {
    return {
      fetchData: () => dispatch(fetchData())
    }
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(DataContainer)
