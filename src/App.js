import React from 'react';
import { Provider } from 'react-redux';
import store from './redux/store'
import DataContainer from './components/DataContainer'
import './App.css';

function App() {
  return (
    <Provider store={store}>
    <div className="App">
    <DataContainer/>
    </div>
    </Provider>
  );
}

export default App;
