import reducer from './Reducer/Reducer'
import { combineReducers } from 'redux'

const rootReducer = combineReducers({
    data:reducer
  })
  
  export default rootReducer