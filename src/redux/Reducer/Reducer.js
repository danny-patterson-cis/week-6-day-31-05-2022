import {
    FETCH_REQUEST,
    FETCH_SUCCESS ,
    FETCH_FAILURE
} from '../Types/Types'

const initialState = {
    loading: false,
    data: null,
    error: null
  }

  const reducer =(state=initialState,action)=>{
      switch (action.type) {
          case FETCH_REQUEST:
              return{
                  ...state,
                  loading:true
              }
              case FETCH_SUCCESS:
                  return{
                      loading:false,
                      data:action.data,
                      error:null
                  }
                  case FETCH_FAILURE:
                      return{
                          loading:false,
                          data:null,
                          error:action.error
                      }
              default: return state
      }
  }

  export default reducer