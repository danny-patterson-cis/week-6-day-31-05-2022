import axios from "axios";
import {
    FETCH_REQUEST,
    FETCH_SUCCESS ,
    FETCH_FAILURE
} from '../Types/Types'

export const myDataRequest =()=>({
    type:FETCH_REQUEST
});

export const myDataSuccess =data=>({
    type:FETCH_SUCCESS,
    data:data,
});

export const myDataFailure =error=>({
    type:FETCH_FAILURE,
    error
});

export const fetchData =()=> {
 return(dispatch)=>{
     dispatch(myDataRequest())
     axios.get('https://jsonplaceholder.typicode.com/photos').then(response =>{
         const data =response.data
         console.log(data)
         dispatch(myDataSuccess(data))
     }).catch(error=>{
         dispatch(myDataFailure(error.message))
     })
 }
}